package com.bbva.tata.lib.rmg1;

import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.domain.transaction.Context;
import com.bbva.elara.domain.transaction.ThreadContext;
import com.bbva.tata.dto.cuentasjson.AccountJSONDTO;
import com.datiobd.daas.DaasMongoConnector;
import com.datiobd.daas.conf.EnumOperation;
import com.datiobd.daas.model.DocumentWrapper;
import com.datiobd.daas.model.FindIterableWrapper;
import com.mongodb.client.MongoCursor;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.Advised;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
		"classpath:/META-INF/spring/TATARMG1-app.xml",
		"classpath:/META-INF/spring/TATARMG1-app-test.xml",
		"classpath:/META-INF/spring/TATARMG1-arc.xml",
		"classpath:/META-INF/spring/TATARMG1-arc-test.xml" })
public class TATARMG1Test {

	private static final Logger LOGGER = LoggerFactory.getLogger(TATARMG1Test.class);

	@Spy
	private Context context;

	@Resource(name = "tataRMG1")
	private TATARMG1 tataRMG1;

	@Resource(name = "applicationConfigurationService")
	private ApplicationConfigurationService applicationConfigurationService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		context = new Context();
		ThreadContext.set(context);
		getObjectIntrospection();
	}
	
	private Object getObjectIntrospection() throws Exception{
		Object result = this.tataRMG1;
		if(this.tataRMG1 instanceof Advised){
			Advised advised = (Advised) this.tataRMG1;
			result = advised.getTargetSource().getTarget();
		}
		return result;
	}

	@Test
	public void executeTest(){	
		LOGGER.info("Executing the test...");
		Assert.assertEquals(0, context.getAdviceList().size());
	}
	
}
