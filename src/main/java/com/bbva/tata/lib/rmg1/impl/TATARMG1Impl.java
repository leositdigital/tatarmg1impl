package com.bbva.tata.lib.rmg1.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.bbva.apx.exception.business.BusinessException;
import com.bbva.tata.dto.cuentasjson.AccountJSONDTO;
import com.datiobd.daas.Parameters;
import com.datiobd.daas.conf.EnumOperation;
import com.datiobd.daas.conf.InsertOneOptionsWrapper;
import com.datiobd.daas.model.DocumentWrapper;
import com.datiobd.daas.model.FindIterableWrapper;
import com.datiobd.daas.model.json.FiltersWrapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * The TATARMG1Impl class...
 */
public class TATARMG1Impl extends TATARMG1Abstract {

	private static final Logger LOGGER = LoggerFactory.getLogger(TATARMG1Impl.class);
	private static final String COLLECTIONNAME = "cuentas";
	private static final String DBASEMONGO = "databasemongo";
	private static final String TATA00000401 = "TATA00000401";

	@Override
	public int executeCreateDocument(AccountJSONDTO accountJsonDTOIn) {
		
		InsertOneOptionsWrapper insertOptions = new InsertOneOptionsWrapper();
		insertOptions.bypassDocumentValidation(false);//omitir la validación de documentos
		Map<String, Object> baseParameters = new HashMap<>();
		baseParameters.put(Parameters.DATABASE_PROPERTY_NAME, DBASEMONGO);//nombre lógico db
		baseParameters.put(Parameters.COLLECTION, COLLECTIONNAME);
		baseParameters.put(Parameters.DOCUMENT, DocumentWrapper.parse(accountJsonDTOIn.toString()));
		baseParameters.put(Parameters.INSERT_ONE_OPTIONS, insertOptions);
		baseParameters.put(Parameters.API_OR_REST, Parameters.API);		
		int response = 1;
		try {
		    this.daasMongoConnector.executeWithNoReturn(EnumOperation.INSERT_ONE, baseParameters);
		}catch(BusinessException ex) {
			LOGGER.info("@@@@@@ BusinessException en el insert {}",ex.getMessage());
			response=0;
			this.addAdvice(TATA00000401);
		}		
		return response;
	}

	@Override
	public List<AccountJSONDTO> executeGetDocuments(AccountJSONDTO accountJsonDTOIn) {
		List<AccountJSONDTO>  response = new ArrayList();
		InsertOneOptionsWrapper insertOptions = new InsertOneOptionsWrapper();
		insertOptions.bypassDocumentValidation(false);//omitir la validación de documentos
		Map<String, Object> baseParameters = new HashMap<>();
		baseParameters.put(Parameters.DATABASE_PROPERTY_NAME,DBASEMONGO);
		baseParameters.put(Parameters.COLLECTION, COLLECTIONNAME);		
		baseParameters.put(Parameters.API_OR_REST, Parameters.API);
		baseParameters.put(Parameters.FILTER, FiltersWrapper.eq("cdDivisa", accountJsonDTOIn.getCdDivisa()));
		
		FindIterableWrapper<DocumentWrapper> result= this.daasMongoConnector.executeWithReturn(EnumOperation.FIND, baseParameters);
		
		for(DocumentWrapper doc:result) {
			JsonObject jsonObject = new JsonParser().parse(doc.toJson()).getAsJsonObject();
			LOGGER.info("@@@@ DocumentWrapper cdDivisa :{}", jsonObject.toString());
			AccountJSONDTO accountJSONDTO = new AccountJSONDTO();
			accountJSONDTO.setNumCuenta(jsonObject.get("numCuenta").getAsString());
			accountJSONDTO.setTpCuenta(jsonObject.get("tpCuenta").getAsString());
			LOGGER.info("@@@@ DocumentWrapper cdDivisa :{}", jsonObject.get("cdDivisa").toString());//'MXN'
			accountJSONDTO.setCdDivisa(jsonObject.get("cdDivisa").getAsString());
			accountJSONDTO.setSaldo(jsonObject.get("saldo").getAsDouble());
			response.add(accountJSONDTO);
		}
		
		long total = this.daasMongoConnector.executeWithReturn(EnumOperation.COUNT, baseParameters);
		LOGGER.info("@@@@ Cuantos registros tenemos  {}", total);		
	    //this.daasMongoConnector.executeWithNoReturn(EnumOperation.FIND, baseParameters);		
		return response;
	}


}
