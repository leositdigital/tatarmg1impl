package com.bbva.tata.lib.rmg1.impl;

import com.bbva.elara.configuration.manager.application.ApplicationConfigurationService;
import com.bbva.elara.library.AbstractLibrary;
import com.bbva.tata.lib.rmg1.TATARMG1;
import com.datiobd.daas.DaasMongoConnector;

/**
 * This class automatically defines the libraries and utilities that it will use.
 */
public abstract class TATARMG1Abstract extends AbstractLibrary implements TATARMG1 {

	protected ApplicationConfigurationService applicationConfigurationService;

	protected DaasMongoConnector daasMongoConnector = new DaasMongoConnector();


	/**
	* @param applicationConfigurationService the this.applicationConfigurationService to set
	*/
	public void setApplicationConfigurationService(ApplicationConfigurationService applicationConfigurationService) {
		this.applicationConfigurationService = applicationConfigurationService;
	}

}